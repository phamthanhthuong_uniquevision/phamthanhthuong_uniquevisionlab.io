module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 2015,
    ecmaFeatures: { legacyDecorators: true }
  },
  extends: [
    // '@nuxtjs',
    // 'plugin:nuxt/recommended',
    "@nuxtjs/eslint-config-typescript"
  ],
  // add your custom rules here
  rules: {
  }
}
