
class GlobalLoadingState {
  isLoading: boolean = false

  loading(func: Promise<void>){
    this.isLoading = true
    func.finally(()=> {
      this.isLoading = false
    })
  }
}

export const globalLoadingState = new GlobalLoadingState()
