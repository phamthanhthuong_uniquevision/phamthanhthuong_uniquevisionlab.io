import { gitlabState } from "./gitlab";
import { globalLoadingState } from "./global_loading";

export default {
  gitlabState: gitlabState,
  globalLoadingState: globalLoadingState
}
