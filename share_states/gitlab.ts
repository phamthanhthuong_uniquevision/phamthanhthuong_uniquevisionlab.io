import { GitlabProject, getGitlabApiInstance, GitlabUser } from "@/api";
import { globalLoadingState } from "./global_loading";
import { UV_CONFIG } from "~/lib/gitlab_config";

interface GitlabProjectState {
  project: GitlabProject
  users: GitlabUser[]
}

class GitlabState {
  gitlabProjectStates: GitlabProjectState[] = []
  private createdFlag = false

  async createShareState(){
    globalLoadingState.loading(this.fetch())
  }

  private async fetch(){
    const gitlabApiInstace = getGitlabApiInstance()
    const projects = await gitlabApiInstace.getProjectsInGroup(UV_CONFIG.groupId)
    await Promise.all(
      projects.map(project => {
        return gitlabApiInstace.getProjectMembers(project.id).then((users: GitlabUser[]) =>{
          this.gitlabProjectStates.push({
            project,
            users
          })
        })
      })
    )
    this.createdFlag = true
  }

  async removeUserFromProject(project: GitlabProject, user: GitlabUser){
    const gitlabApiInstace = getGitlabApiInstance()
    await gitlabApiInstace.removeUserInProject(project.id, user.id)

    const gitlabProjectState = this.gitlabProjectStates.find(item => item.project.id == project.id)
    if (gitlabProjectState) {
      const userIndex = gitlabProjectState.users.findIndex(item => item.id === user.id)
      if (userIndex > -1) {
        gitlabProjectState.users.splice(userIndex, 1)
      }
    }
    // Todo
  }

  async removeUser(user: GitlabUser){
    const userGitlabProjects = this.gitlabProjectStates.filter(project => project.users.find(item => item.id === user.id))

    await Promise.all(
      userGitlabProjects.map(userGitlabProject => {
        return this.removeUserFromProject(userGitlabProject.project, user)
      })
    )
  }

  get users() : GitlabUser[] {
    const users: GitlabUser[] = [];
    this.gitlabProjectStates.forEach(gitlabProjectState => {
      gitlabProjectState.users.forEach(user => {
        const found = users.find(item => item.id === user.id)
        if (!found) {
          users.push(user)
        }
      })
    });
    return users
  }

  get projects(): GitlabProject[]{
    return this.gitlabProjectStates.map(item => item.project)
  }

  get isCreated(){
    return this.createdFlag;
  }
}

export const gitlabState = new GitlabState()

